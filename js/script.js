
// OUR SERVICE section

$('.service-tab .tab-item').on('click', function(){
    $('.service-tab .tab-item').removeClass('active arrowtab')
    $(this).addClass('active arrowtab')
    const selector = $(this).attr('data-target')
    $('.tabcontent').removeClass('active')
    $(selector).addClass('active')
})


//  WHAT PEOPLE SAY section

const allSlides = document.querySelectorAll(".blog-slide");
const slidesLastElementIndex = allSlides.length - 1;
const allSliderItems = document.querySelectorAll(".blog-slider-item");

const sliderNextArrow = document.querySelector(".arrow.arrow-right");
sliderNextArrow.addEventListener("click", function () {
    const currentActiveSlide = findActiveElement(allSlides, "active");
    const currentActiveSlideIndex = findElementIndex(allSlides, currentActiveSlide);
    let nextActiveSlideIndex = currentActiveSlideIndex + 1;
    if (nextActiveSlideIndex > slidesLastElementIndex) {
        nextActiveSlideIndex = 0;
    }
    setActiveElement(allSlides, nextActiveSlideIndex, "active");
    setActiveElement(allSliderItems, nextActiveSlideIndex, "active");
});

const sliderPrevArrow = document.querySelector(".arrow.arrow-left");
sliderPrevArrow.addEventListener("click", function () {
    const currentActiveSlide = findActiveElement(allSlides, "active");
    const currentActiveSlideIndex = findElementIndex(allSlides, currentActiveSlide);
    let nextActiveSlideIndex = currentActiveSlideIndex - 1;
    if (nextActiveSlideIndex < 0) {
        nextActiveSlideIndex = slidesLastElementIndex;
    }
    setActiveElement(allSlides, nextActiveSlideIndex, "active");
    setActiveElement(allSliderItems, nextActiveSlideIndex, "active");
});

allSliderItems.forEach(item => {
    item.addEventListener("click", function () {
        const currentElementIndex = findElementIndex(allSliderItems, this);
        setActiveElement(allSliderItems, currentElementIndex, "active");
        setActiveElement(allSlides, currentElementIndex, "active");
    });
});


function findActiveElement(allElements, activeElementClass) {
    let activeElementIndex = 0;
    for (let i = 0; i < allElements.length; i++) {
        if (allElements[i].classList.contains(activeElementClass)) {
            activeElementIndex = i;
            break;
        }
    }
    return allElements[activeElementIndex];
}

function findElementIndex(elementsList, element) {
    let elementIndex = 0;
    for (let i = 0; i < elementsList.length; i++) {
        if (elementsList[i] === element) {
            elementIndex = i;
            break;
        }
    }
    return elementIndex;
}

function setActiveElement(elementList, index, activeClass) {
    for (let i = 0; i < elementList.length; i++) {
        elementList[i].classList.remove(activeClass);
    }
    elementList[index].classList.add(activeClass);
}

// OUR WORKS section

const tabList = document.querySelectorAll('.works-list .works-item')
tabList.forEach(elem =>
    elem.addEventListener('click', function({target}){
        const prevActiveTab = document.querySelector('.works-item.active')
        prevActiveTab.classList.remove('active')
        target.classList.add('active')
        const selector = this.dataset.filter
        const allCards = document.querySelectorAll('.card')
        allCards.forEach(item => item.classList.remove('active'))
        const selectedCards = document.querySelectorAll( selector)
        selectedCards.forEach(item =>item.classList.add('active'))
    }))

const btnWorksSection = document.querySelector('.our-amazing-works .btn.load-more')
btnWorksSection.addEventListener('click', function(e){
    e.preventDefault()
    showLoader('#cube-loader')
    setTimeout(hideLoader, 3000, '#cube-loader')
    setTimeout(showCards, 3500)
    this.remove()
})
function showCards(){
    const cards = document.querySelectorAll('.img-block .card')
    cards.forEach(elem =>elem.classList.add('active', 'first'))
}
function showLoader(selector){
    document.querySelector(selector).style.display = 'block'
}
function hideLoader(selector){
    document.querySelector(selector).style.display = 'none'
}

// GALLERY section

const container = document.querySelector('.img-container');
const msnr = new Masonry(container, {
    columnWidth: 370,
    itemSelector: '.item',
    gutter: 20
});

const btn = document.querySelector('.gallery .btn.load-more')
btn.addEventListener('click', function (e) {
    e.preventDefault()
    showLoader('.gallery #cube-loader')
    setTimeout(hideLoader, 3000, '.gallery #cube-loader')
    setTimeout(showPic, 3500)
    this.remove()
})
function showPic(){
    const images = document.querySelectorAll('.item.new')
    images.forEach(item =>item.classList.add('active'))
    const container = document.querySelector('.img-container');
    const msnr = new Masonry(container, {
        columnWidth: 370,
        itemSelector: '.item',
        gutter: 20
    });
}
